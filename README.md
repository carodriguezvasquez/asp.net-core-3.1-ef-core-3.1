# Asp.Net Core MVC + Entity Framework Core + Repository pattern and Unit of Work 

# Asp.Net Core 3.1

"ASP.NET Core es un framework web gratis de código abierto y con un mayor rendimiento que ASP.NET,3​ desarrollado por Microsoft y la comunidad.4​ Es un framework modular que se ejecuta completo tanto en el.NET Framework de Windows como en multiplataforma. .NET Core. Sin embargo ASP.NET versión Core 3 sólo funciona en .NET Core dejando el soporte de .NET Framework.5​". 
--Wikipedia

# Repository and Unit of Work

"Los patrones de repositorio y unidad de trabajo están destinados a crear una capa de abstracción entre la capa de acceso a datos y la capa de lógica empresarial de una aplicación. La implementación de estos patrones puede ayudar a aislar su aplicación de los cambios en el almacén de datos y puede facilitar las pruebas unitarias automatizadas o el desarrollo basado en pruebas (TDD)". 
--Microsoft.

# Entity Framework Core

(EF) es un asignador relacional de objetos (ORM) que permite a los desarrolladores de .NET trabajar con datos relacionales.

En este proyecto de demostración utilizará instancia de base de datos local DB SQL Server. Es posible instalar otros [proveedores de servicios](https://docs.microsoft.com/en-us/ef/core/providers/?tabs=dotnet-core-cli) de Entity Framework Core.

### Notas: 
1. Este ejercicio esta desarrollado en un proyecto de Asp.net Core **3.1** MVC.
2. La versión de Entity Framework Core que estoy utilzando la **3.1.10**.
3. Este proyecto tiene codigo de ejemplo para crear, listar y eliminar una entidad llamada clase.
4. La conexión a la base de datos es local db.
5. Agregue una migración con el comando: add-migration "Nombre migracion" (sin comillas).

## Instalación EF Core 3.1

#### Proveedor Sql Server
Manage Nuget packages:
`microsoft.entityframeworkcore.sqlserver`; buscar, descargar e instalar.

#### Tools Entity Framework
Manage Nuget packages:
`microsoft.entityframeworkcore.tools`; buscar, descargar e instalar.

Una vez instalada hablita [comandos](https://docs.microsoft.com/en-us/ef/core/cli/powershell) especiales desde el que permiten aplicar generar apartir de las clases las tablas y relaciones de la base de datos.
